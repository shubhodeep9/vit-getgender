from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash
from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired, Required
import urllib2
import io, json

def vitGender(regno):
    url = 'http://apius.faceplusplus.com/v2/detection/detect?api_key=e2707513a30c55f950583457e8845ec1&api_secret=9cWd6oDOtFMmqhGT7mwPKphefakx52tI&url=https%3A%2F%2Facademics.vit.ac.in%2Fstudent%2Fview_photo_2.asp%3Frgno%3D'+str(regno)+'&attribute=age%2Cgender%2Crace%2Csmiling%2Cpose%2Cglass'
    page = urllib2.urlopen(url)
    data = json.load(page)
    gender= data['face'][0]['attribute']['gender']['value']
    return gender

DEBUG = True
SECRET_KEY = 'development key'

app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('TRAVELSAFE_SETTINGS', silent=True)


class MyForm(Form):
    name = StringField('Reg No', validators=[DataRequired()])

@app.route('/')
def index():
    form = MyForm()
    return render_template('index.html', form=form)

@app.route('/submit', methods = ['GET','POST'])
def submit():
    error = None
    if request.method == 'POST':
        inp = request.form['name']
        gender = vitGender(inp)
        return 'The gender is : ' + gender
    return error
if __name__ == '__main__':
    app.run()
